import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

// Main component
class App extends React.Component {
  constructor() {
    super();
    this.state = {
      team : [],
      newPlayer : {
        id: '',
        name: '',
        position : ''
      }
    }
  }

  handleNameChange(e) {
    const currentPlayer = this.state.newPlayer
    currentPlayer.name = e.target.value
    this.setState({
      newPlayer: currentPlayer
    })
  }

  handlePositionChange(e) {
    const currentPlayer = this.state.newPlayer

    currentPlayer.position = e.target.value
    this.setState({
      newPlayer: currentPlayer
    })

  }

  handleClick() {
    // Validation
    let pPos = this.state.newPlayer.position
    let posCondition = (+pPos < 1) || (+pPos > 15) || (isNaN(+pPos))

    let pName = this.state.newPlayer.name
    let nameCondition = (this.state.newPlayer.name == "")
    if (nameCondition) {
      alert('Please enter a name')
    } else if (posCondition) {
      alert('Please enter a valid position')
    } else {

      const id = this.state.team.length
      const currentPlayer = this.state.newPlayer
      currentPlayer.id = id + currentPlayer.name + currentPlayer.position
      this.setState({
        newPlayer: currentPlayer
      })

      var currentTeam = this.state.team.slice(0)
      currentTeam[id] = this.state.newPlayer

      this.setState({
        team : currentTeam,
        newPlayer : {
          id: '',
          name: '',
          position : ''
        }
      })
    }
  }

  render() {
    return (
    <div className='app-main'>
      <div className='app-form'>
        <form>
          <input id='playerNameInput' type='text' name='name' placeholder='Name' value={this.state.newPlayer.name} onChange={(e)=>this.handleNameChange(e)} autoFocus/>
          <input id='playerPositionInput' type='text' name='position' placeholder='#' value={this.state.newPlayer.position} onChange={(e)=>this.handlePositionChange(e)}/>
          <input className='btn-primary' type='button' value='Add' onClick={()=>this.handleClick()}/>
        </form>
      </div>
      <div className='app-teamlist'>
        <Team team={this.state.team}/>
      </div>
    </div>
    )
  }
}

class Team extends React.Component {
  handleDeletePlayer(p) {
    const newTeam = this.props.team
    let index = newTeam.indexOf(p)
    newTeam.splice(index, 1)
    console.log(newTeam)
    this.setState({
      team:newTeam})

  }


  render() {
    const team = this.props.team.slice(0)
    const playerList = team.map((p)=>{
      return <li key={p.id}>{p.position}. {p.name} <a href="#" onClick={this.handleDeletePlayer.bind(this, p)}>X</a></li>
    })

    return (
      <div className="team-main">
       <ul>{playerList}</ul>
    </div>
    );
  }
}

ReactDOM.render(<App/>, document.getElementById('root'))
